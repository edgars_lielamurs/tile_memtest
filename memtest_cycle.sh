#!/pckg/option/bin/ash
#
# Original author: Edgars Lielāmurs
#
# MIT License
# 
# Copyright (c) 2020 
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# This script is intended for tilera based system memeory testing.
# Multiple memtester processes are executed according to core count.
# Heap size is calculated based on free memory.
# Only failure messages are displayed.
#
# Usage: copy to DUT and ./memtest_cycle 

#------------------------------------------------------------------------------#
# Functions                                                                    #   
#------------------------------------------------------------------------------#

tile_core_count ( )
{
  local cores
  cores=`grep "count" /proc/cpuinfo | sed 's/[^0-9]//g'`
  echo $cores  
}

free_mem ( )
{
  local freem
  freem=`cat /proc/meminfo | grep MemFree | sed 's/[^0-9]//g'`
  echo $freem
}

print_style () {
    RED='\033[0;31m'
    NC='\033[0m' # No Color
    if [ "$2" = "info" ] ; then
        COLOR="96m";
    elif [ "$2" = "success" ] ; then
        COLOR="92m";
    elif [ "$2" = "warning" ] ; then
        COLOR=$RED;
    elif [ "$2" = "danger" ] ; then
        COLOR="91m";
    else #default color
        COLOR="0m";
    fi

    STARTCOLOR=$COLOR;
    ENDCOLOR=$NC;

    printf "${STARTCOLOR}%b${ENDCOLOR}\n" "$1";
}

#------------------------------------------------------------------------------#
# Variables                                                                    #   
#------------------------------------------------------------------------------#

cores=`tile_core_count`
memfree=`free_mem`

print_style "${YELLOW}INFO: Core count : $cores${NC} \n"
if [ $cores = "36" ] ; then \
    heap=$((($memfree-175000)/36/1024)); \
    printf "Heap size :  %d MB\n" $heap; \
	sleep 5; \
	printf "Starting 36 memtests with heap size %d MB ...\n" $heap; \
	for i in `seq 0 35`; do \
	memtester $heap -o | grep "FAILURE" & \
    sleep 1; \
    done; \
	printf "All memtests are running.\n"
elif [ `grep "count" /proc/cpuinfo | sed 's/[^0-9]//g'` = "9" ] ; then \
	heap=$((($memfree-75000)/9/1024)); \
    printf "Heap size :  %d MB\n" $memfree; \
	sleep 5; \
	printf "Starting 9 memtests with heap size %d MB ...\n" $heap; \
	for i in `seq 0 8`; do \
	memtester $heap -o | grep "FAILURE" & \
    sleep 1; \
    done; \
	printf "All memtests are running.\n"
elif [ `grep "count" /proc/cpuinfo | sed 's/[^0-9]//g'` = "16" ] ; then \
    heap=$((($memfree-175000)/16/1024)); \
    printf "Heap size :  %d MB\n" $heap; \
    sleep 5; \
    printf "Starting 16 memtests with heap size %d MB ...\n" $heap; \
    for i in `seq 0 16`; do \
	memtester $heap -o | grep "FAILURE" & \
    sleep 1; \
    done; \
    printf "All memtests are running.\n"
elif [ $cores = "72" ] ; then \
    heap=$((($memfree-200000)/72/1024)); \
    printf "Heap size :  %d MB\n" $heap; \
	sleep 5; \
	printf "Starting 72 memtests with heap size %d MB ...\n" $heap; \
	for i in `seq 0 71`; do \
	memtester $heap -o | grep "FAILURE" & \
        sleep 1; \
    done; \
	printf "All memtests are running.\n"
fi
